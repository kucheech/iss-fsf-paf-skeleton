const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

//start sequelize
const DB_NAME = "iss-fsf";
const DB_USER = "root";
const DB_PASSWORD = "password";
const DB_HOST = "localhost"
const Sequelize = require("sequelize");
const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD,
    {
        host: DB_HOST,
        dialect: 'mysql',
        logging: console.log,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        define: {
            timestamps: false
        }
    }
);
const Accounts = require("./models/accounts")(sequelize, Sequelize);


const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));

app.get("/accounts", function (req, res) {
    Accounts.findAll().then(accounts => {
        res.status(200).json(accounts);
    })
});

app.get("/accounts/:id", function (req, res) {
    const id = req.params.id;
    Accounts.findById(id).then(account => {
        res.status(200).json(account);
    }).catch(err => {
        handleError(err, res);
    });
});

//add account
app.post("/accounts", function (req, res) {
    const account = req.body.account;
    console.log(account);
    Accounts.findOrCreate({ where: { email: account.email }, defaults: account })
        .spread((a, created) => {
            if (created) {
                res.status(201).send("Account added to database");
            } else {
                res.status(422).send("Email already exists in database");
            }
        }).catch(err => {
            handleError(err, res);
        });

});


//update account
app.put("/accounts/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    const account = req.body.account;

    Accounts.update(account, { where: { id: id } }).then(result => {
        res.status(200).send(result);
    }).catch(err => {
        console.log(err);
        res.status(500).send(err);
    });
});

//delete a particular account based on id
app.delete("/accounts/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    // console.log(id);
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    Accounts.destroy({ where: { id: id } }).then(result => {
        res.status(200).end();
    }).catch(function (err) {
        console.log(err);
        res.status(500).send(err);
    });
});


var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'iss-fsf'
});


// //using q
const mkQuery = function (sql, pool) {

    const sqlQuery = function () {
        const defer = q.defer();

        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, sqlParams, function (err, result) {
                if (err) {
                    defer.reject(err);
                    console.log(err);
                } else {
                    // console.log(result);
                    defer.resolve(result);
                }
                conn.release();
            });
        });

        return defer.promise;
    }

    return sqlQuery;
};

const SELECT_ALL_USERS = "select * from users";
const SELECT_USER_BY_ID = "select * from users where regid = ? limit 1";
const UPDATE_USER_BY_ID = "update users set name = ?, email = ?, phone = ?, dob = ? where regid = ?";


const getAllUsers = mkQuery(SELECT_ALL_USERS, pool);
const getUserById = mkQuery(SELECT_USER_BY_ID, pool);
const updateUserById = mkQuery(UPDATE_USER_BY_ID, pool);

function formatDate(date1) {
    return date1.getFullYear() + '-' +
        (date1.getMonth() < 9 ? '0' : "") + (date1.getMonth() + 1) + '-' +
        (date1.getDate() < 10 ? '0' : "") + date1.getDate();
}

const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});


app.post("/user/:id", function (req, res) {
    const id = req.params.id;
    const user = req.body.user;
    updateUserById(user.name, user.email, user.phone, formatDate(new Date(user.dob)), id)
        .then(function (result) {
            console.log(result);
            // res.status(404).type("text/plain").send("Customer not found");
            res.status(200).end();
        }).catch(function (err) {
            handleError(err, res);
        });
});



app.get("/users", function (req, res) {
    getAllUsers().then(function (result) {
        if (result.length > 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send("No users");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

app.get("/user/:id", function (req, res) {
    const id = req.params.id;
    getUserById(id).then(function (result) {
        if (result.length > 0) {
            var book = result[0];
            res.status(200).json(book);
        } else {
            res.status(404).send("User not found");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app