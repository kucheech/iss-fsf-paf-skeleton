(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q"];

    function MyAppService($http, $q) {
        var service = this;

        //expose the following services
        service.getAccounts = getAccounts;
        service.getAccountById = getAccountById;
        service.updateAccount = updateAccount;

        function updateAccount(account) {
            var defer = $q.defer();
            const id = user.id;
            $http.post("/accounts/" + id, { account: account })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getAccountById(id) {
            var defer = $q.defer();

            $http.get("/accounts/" + id).then(function (result) {
                // console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function getAccounts() {
            var defer = $q.defer();

            $http.get("/accounts").then(function (result) {
                console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }
        
    }

})();