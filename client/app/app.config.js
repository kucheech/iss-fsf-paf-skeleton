(function () {
    "use strict";
    angular.module("MyApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function MyConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("accounts", {
                url: "/accounts",
                templateUrl: "/views/accounts.html",
                controller: "AccountsCtrl",
                controllerAs: "ctrl"
            })
            .state("edit", {
                url: "/edit/:id",
                templateUrl: "/views/edit.html",
                controller: "EditCtrl",
                controllerAs: "ctrl"
            })

        $urlRouterProvider.otherwise("/accounts");
    }





})();